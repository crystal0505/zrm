系统的开发环境
HBuilder X、MySQL、GeoServer、PostGIS、PostgreSQL

开发语言
JavaScript、HTML、CSS

部署的步骤
1.将GeoServer配置到Tomcat下，运行startup.bat文件，部署GeoServer。安装PostgreSQL和PostGIS，将处理好的地图数据存储至PostGIS中，再将PostGIS中的数据发布至GeoServer地图服务器中。
2.在项目中新建index.js文件，进行数据库配置和后端服务配置，使用node运行该文件。
3.前两项启动成功后，运行vue项目，进入登录界面。
4.输入正确的用户信息进入主界面，点击菜单栏按钮实现相应功能。