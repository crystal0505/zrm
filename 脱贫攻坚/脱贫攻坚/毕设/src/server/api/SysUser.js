const express = require('express')
const router = express.Router()
const mysql = require('mysql')
const crypto = require('crypto')
 
let conn = mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'721400',
    database:'tpgj'
});
conn.connect(function(err){
	if(err){
		console.log("error")
	}
	console.log("连接成功！")
})

const jsonWrite = function (res, ret) {
  if (typeof ret === 'undefined') {
    res.json({
      code: '1', msg: '操作失败'
    })
  } else {
    res.json(
      ret
    )
  }
}
 
// 接口：增加信息sql,编辑修改信息sql1
router.post('/add1', (req, res) => {
  const params = req.body
  const sql = "insert into 先进典型(姓名,年龄,原工作地,帮扶地区,所获荣誉,主要成就,事迹简介) values (?,?,?,?,?,?,?)"
  conn.query(sql, [params.姓名, params.年龄,params.原工作地,params.帮扶地区,params.所获荣誉,params.主要成就,params.事迹简介], function (err, result) {
      if (err) {
        console.log(err)
      }
      if (result) {
        jsonWrite(res, result)
      }
    })
  
})

router.post('/add2', (req, res) => {
  const params = req.body
  const sql = "insert into 走访足迹(地名,所属省份,走访时间,经度,纬度,简介) values (?,?,?,?,?,?)"
  conn.query(sql, [params.地名, params.所属省份,params.走访时间,params.经度,params.纬度,params.简介], function (err, result) {
      if (err) {
        console.log(err)
      }
      if (result) {
        jsonWrite(res, result)
      }
    })
  
})
 
 router.post('/update1', (req, res) => {
   const params = req.body
   const sql1 = `update 先进典型 set 帮扶地区 = ?,年龄= ?,原工作地 = ?,所获荣誉=?,主要成就=?,事迹简介=?where 姓名 = ?`
     conn.query(sql1, [params.帮扶地区,params.年龄, params.原工作地,  params.所获荣誉, params.主要成就,params.事迹简介,params.姓名,], function (err, result) {
       if (err) {
         console.log(err)
       }
       if (result) {
         jsonWrite(res, result)
       }
     })
   
 })
 
 router.post('/update2', (req, res) => {
   const params = req.body
   const sql1 = `update 走访足迹 set 所属省份 = ?,走访时间= ?,经度 = ?,纬度=?,简介=?where 地名 = ?`
     conn.query(sql1, [params.所属省份,params.走访时间, params.经度,  params.纬度, params.简介,params.地名,], function (err, result) {
       if (err) {
         console.log(err)
       }
       if (result) {
         jsonWrite(res, result)
       }
     })
   
 })
 
 router.post('/que1', (req, res) => {
   const params = req.query
   const sql = "SELECT * FROM 先进典型 where 姓名='"+params.姓名+"'"
   conn.query(sql, params,function (err, result) {
     if (err) {
       console.log(err)
     }
     if (result) {
		 jsonWrite(res, result)
     }
   })
 })
 
 router.post('/que2', (req, res) => {
   const params = req.query
   const sql = "SELECT * FROM 走访足迹 where 地名='"+params.地名+"'"
   conn.query(sql, params,function (err, result) {
     if (err) {
       console.log(err)
     }
     if (result) {
 		 jsonWrite(res, result)
     }
   })
 })
// 接口：用户管理分页接口查询
router.get('/getlist', (req, res) => {
  const params = req.query
  const sql = "SELECT * FROM 先进典型"
  const sql2 = `SELECT COUNT(*) as total FROM 先进典型`
  conn.query(sql, function (err, result) {
    if (err) {
      console.log(err)
    }
    if (result) {
      conn.query(sql2,function(err, result2) {
        if(err){
          console.log(err)
        }
        if(result2){
          let ret = {
            data:result,
            total:result2[0].total
          }
          jsonWrite(res, ret)
        }
      })
    }
  })
})
 
 router.get('/footprint', (req, res) => {
   const params = req.query
   const sql = "SELECT * FROM 走访足迹"
   const sql2 = `SELECT COUNT(*) as total FROM 走访足迹`
   conn.query(sql, function (err, result) {
     if (err) {
       console.log(err)
     }
     if (result) {
       conn.query(sql2,function(err, result2) {
         if(err){
           console.log(err)
         }
         if(result2){
           let ret = {
             data:result,
             total:result2[0].total
           }
           jsonWrite(res, ret)
         }
       })
     }
   })
 })
 
 router.get('/GDP', (req, res) => {
   const params = req.query
   const sql = "SELECT 2010年人均GDP,2020年人均GDP FROM 全国各省贫困情况表"
 
   conn.query(sql, function (err, result) {
     if (err) {
       console.log(err)
     }
     if (result) {
		 let ret = {
		   data:result
		 }
		  jsonWrite(res, ret)
     }
   })
 })
 
 router.get('/pkfsl', (req, res) => {
   const params = req.query
   const sql = "SELECT 省份,贫困发生率 FROM 全国各省贫困情况表"
 
   conn.query(sql, function (err, result) {
     if (err) {
       console.log(err)
     }
     if (result) {
 		 let ret = {
 		   data:result
 		 }
 		  jsonWrite(res, ret)
     }
   })
 })
// 接口：删除单条数据
router.post('/del1', (req, res) => {
  const params = req.body
  const sql = "DELETE FROM 先进典型 where 姓名=?"
  conn.query(sql, [params.姓名], function (err, result) {
    if (err) {
      console.log(err)
    }
    if (result) {
      jsonWrite(res, result)
    }
  })
})

router.post('/del2', (req, res) => {
  const params = req.body
  const sql = "DELETE FROM 走访足迹 where 地名=?"
  conn.query(sql, [params.地名], function (err, result) {
    if (err) {
      console.log(err)
    }
    if (result) {
      jsonWrite(res, result)
    }
  })
})
 
// 接口：登录
router.post('/login', (req, res) => {
  const params = req.body
  const sql = "SELECT * FROM 账户信息 where 用户名='"+params.用户名+"'and 密码='"+params.密码+"'"
  conn.query(sql, params, function (err, result) {
    if (err) {
      console.log(err)
    }
    if (result) {
      jsonWrite(res, result)
    }
  })
})
module.exports = router