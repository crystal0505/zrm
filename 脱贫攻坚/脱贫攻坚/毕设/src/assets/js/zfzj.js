$(function(){
	//查询
	var que2=document.getElementById("que2");
	que2.onclick=function(){
		var wbk2=document.getElementById("wbk2").value;
		var cx2=document.getElementById("cx2");
		var dm2=document.getElementById("dm2");
		var sssf2=document.getElementById("sssf2");
		var zfsj2=document.getElementById("zfsj2");
		var lat2=document.getElementById("lat2");
		var lon2=document.getElementById("lon2");
		var intro2=document.getElementById("intro2");
		axios({
			method:"POST",
			url:'http://localhost:3000/api/sysuser/que2',
			params:{地名:wbk2}
		}).then(response => {
			console.log(response);
			if (response.status == 200) {
				cx2.style.display="";
			    let datas = response.data
				let data=datas[0]
				dm2.value=data.地名;
				sssf2.value=data.所属省份;
				zfsj2.value=data.走访时间;
				lat2.value=data.经度;
				lon2.value=data.纬度;
				intro2.value=data.简介;
				var b22=document.getElementById("b22");
				b22.onclick=function(){
				cx2.style.display="none";
				}
			}
			 else{
				 console.log(response);
			    }
			})
			.catch(error => {
			    console.log(error);
				cx2.style.display="none";
			})   
	}
	
	//添加
	var add2=document.getElementById("add2");
	add2.onclick=function(){
		var tj2=document.getElementById("tj2");
		tj2.style.display="";
		var b21=document.getElementById("b21");
		b21.onclick=function(){
			var dm1=document.getElementById("dm1").value;
			var sssf1=document.getElementById("sssf1").value;
			var zfsj1=document.getElementById("zfsj1").value;
			var lat1=document.getElementById("lat1").value;
			var lon1=document.getElementById("lon1").value;
			var intro1=document.getElementById("intro1").value;
			let param = new URLSearchParams()
			param.append('地名', dm1)
			param.append('所属省份', sssf1)
			param.append('走访时间', zfsj1)
			param.append('经度', lat1)
			param.append('纬度', lon1)
			param.append('简介', intro1)
			axios({
				method:"POST",
				url:'http://localhost:3000/api/sysuser/add2',
				data:param
				// data:{
				// 	地名:dm1,
				// 	所属省份:sssf1,
				// 	走访时间:zfsj1,
				// 	经度:lat1,
				// 	纬度:lon1,
				// 	简介:intro1
				// },
			}).then(response => {
				console.log(response);
				if (response.status == 200) {
				   console.log("添加成功")
				   tj2.style.display="none";
				}
				 else{
					 console.log(response)
				    }
				})
				.catch(error => {
				    console.log(error)
				})  
		}
	}
	
	//修改
	var update2=document.getElementById("update2");
	update2.onclick=function(){
		var xg2=document.getElementById("xg2");
		xg2.style.display="";
		var wbk2=document.getElementById("wbk2").value;
		var dm3=document.getElementById("dm3");
		var sssf3=document.getElementById("sssf3");
		var zfsj3=document.getElementById("zfsj3");
		var lat3=document.getElementById("lat3");
		var lon3=document.getElementById("lon3");
		var intro3=document.getElementById("intro3");
		axios({
			method:"POST",
			url:'http://localhost:3000/api/sysuser/que2',
			params:{地名:wbk2}
		}).then(response => {
			console.log(response);
			if (response.status == 200) {
				xg2.style.display="";
			    let datas = response.data
				let data=datas[0]
				dm3.value=data.地名;
				sssf3.value=data.所属省份;
				zfsj3.value=data.走访时间;
				lat3.value=data.经度;
				lon3.value=data.纬度;
				intro3.value=data.简介;
			}
			 else{
				 console.log(response);
			    }
			})
			.catch(error => {
			    console.log(error);
				xg2.style.display="none";
			})   
	}
	var b23=document.getElementById("b23");
	b23.onclick=function(){
		var xg2=document.getElementById("xg2");
		var dm3=document.getElementById("dm3").value;
		var sssf3=document.getElementById("sssf3").value;
		var zfsj3=document.getElementById("zfsj3").value;
		var lat3=document.getElementById("lat3").value;
		var lon3=document.getElementById("lon3").value;
		var intro3=document.getElementById("intro3").value;
		axios({
			method:"POST",
			url:'http://localhost:3000/api/sysuser/update2',
			//data:param
			data:{
				    地名:dm3,
					所属省份:sssf3,
					走访时间:zfsj3,
					经度:lat3,
					纬度:lon3,
					简介:intro3
			},
		}).then(response => {
			console.log(response);
			if (response.status == 200) {
			   console.log("修改成功")
			   xg2.style.display="none";
			}
			 else{
				 console.log(response)
			    }
			})
			.catch(error => {
			    console.log(error)
			}) 
	}
	
	//删除
	var delete2=document.getElementById("delete2");
	delete2.onclick=function(){
		var wbk2=document.getElementById("wbk2").value;
		axios({
				method:"POST",
				url:'http://localhost:3000/api/sysuser/del2',
				data:{
					地名:wbk2
					}
			}).then(response => {
				console.log(response);
				if (response.status == 200) {
				    console.log("删除成功")
				}
				 else{
					 console.log(response)
				    }
				})
				.catch(error => {
				    console.log(error)
				})   
	}
})