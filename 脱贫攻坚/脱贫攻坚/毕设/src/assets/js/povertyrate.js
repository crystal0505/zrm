$(function () {
    var btn1 = document.getElementById("btn1")
	var echartslayer;
    btn1.onclick=function(){
		var res = {
		            "locations": [{
		                    name: "北京",
		                    value: 0
		                }, {
		                    name: "天津",
		                    value: 0
		                }, {
		                    name: "山西",
		                    value: 13.6
		                },
		                {
		                    name: "内蒙古",
		                    value: 11.7
		                }, {
		                    name: "河北",
		                    value: 6.5
		                }, {
		                    name: "辽宁",
		                    value: 5.4
		                },
		                {
		                    name: "吉林",
		                    value: 5.9
		                }, {
		                    name: "上海",
		                    value: 0
		                }, {
		                    name: "江苏",
		                    value: 2
		                },
		                {
		                    name: "浙江",
		                    value: 1.9
		                }, {
		                    name: "安徽",
		                    value: 12.6
		                }, {
		                    name: "福建",
		                    value: 2.6
		                },
		                {
		                    name: "江西",
		                    value: 9.21
		                }, {
		                    name: "山东",
		                    value: 3.7
		                }, {
		                    name: "河南",
		                    value: 9.28
		                },
		                {
		                    name: "湖北",
		                    value: 8
		                }, {
		                    name: "湖南",
		                    value: 13.43
		                }, {
		                    name: "广东",
		                    value: 1.7
		                },
		                {
		                    name: "广西",
		                    value: 14.9
		                }, {
		                    name: "海南",
		                    value: 11.1
		                }, {
		                    name: "重庆",
		                    value: 7.1
		                },
		                {
		                    name: "四川",
		                    value: 9.6
		                }, {
		                    name: "贵州",
		                    value: 26.8
		                }, {
		                    name: "云南",
		                    value: 18.79
		                },
		                {
		                    name: "西藏",
		                    value: 28.8
		                }, {
		                    name: "陕西",
		                    value: 15.1
		                }, {
		                    name: "甘肃",
		                    value: 23.8
		                },
		                {
		                    name: "青海",
		                    value: 19.1
		                }, {
		                    name: "宁夏",
		                    value: 12.5
		                }, {
		                    name: "新疆",
		                    value: 19.8
		                },
		                {
		                    name: "黑龙江",
		                    value: 5.9
		    
		                }
		            ],
		            "coordinates": {
		                '北京': [116.4551, 40.2539],
		                '天津': [117.4219, 39.4189],
		                '山西': [112.3352, 37.9413],
		                '内蒙古': [110.3467, 41.4899],
		                '河北': [114.4995, 38.1006],
		                '辽宁': [123.1238, 42.1216],
		                '吉林': [125.8154, 44.2584],
		                '上海': [121.4648, 31.2891],
		                '江苏': [118.8062, 31.9208],
		                '浙江': [119.5313, 29.8773],
		                '安徽': [117.29, 32.0581],
		                '福建': [119.4543, 25.9222],
		                '江西': [116.0046, 28.6633],
		                '山东': [117.0204, 36.6685],
		                '河南': [113.4668, 34.6234],
		                '湖北': [114.3896, 30.6628],
		                '湖南': [113.0823, 28.2568],
		                '广东': [113.5107, 23.2196],
		                '广西': [108.479, 23.1152],
		                '海南': [110.3893, 19.8516],
		                '重庆': [108.384366, 30.439702],
		                '四川': [103.9526, 30.7617],
		                '贵州': [106.6992, 26.7682],
		                '云南': [102.9199, 25.4663],
		                '西藏': [91.1172, 29.6469],
		                '陕西': [109.1162, 34.2004],
		                '甘肃': [103.5901, 36.3043],
		                '青海': [101.4038, 36.8207],
		                '宁夏': [106.3586, 38.1775],
		                '新疆': [87.9236, 43.5883],
		                '黑龙江': [127.9688, 45.368]
		            }
		        }
		        var data = res.locations;
		        var geoCoordMap = res.coordinates;
		        var convertData = function (data) {
		            var res = [];
		            for (var i = 0; i < data.length; i++) {
		                var geoCoord = geoCoordMap[data[i].name];
		                if (geoCoord) {
		                    res.push({
		                        name: data[i].name,
		                        value: geoCoord.concat(data[i].value)
		                    });
		                }
		            }
		            return res;
		        };
		        var option = {
		            title: {
		                text: '贫困程度',
		                subtext: '',
		                sublink: '',
		                left: 'center',
		                textStyle: {
		                    color: '#fff'
		                }
		            },
		            tooltip: {
		                trigger: 'item'
		            },
		            openlayers: {},
		            //legend: {
		            //    orient: 'vertical',
		            //    y: 'top',
		             //   x: 'right',
		            //    data: ['贫困发生率'],
		            //    textStyle: {
		            //        color: '#fff'
		            //    }
		            //},
		            series: [{
		                    name: '贫困发生率',
		                    type: 'scatter',
		                    data: convertData(data),
		                    symbolSize: function (val) {
		                        return val[1]/2 ;
		                    },
		                    label: {
		                        normal: {
		                            formatter: '{b}',
		                            position: 'right',
		                            show: false
		                        },
		                        emphasis: {
		                            show: true
		                        }
		                    },
		                    itemStyle: {
		                        normal: {
		                            color: '#ddb926'
		                        }
		                    }
		                },
		                {
		                    name: 'Top 5',
		                    type: 'effectScatter',
		                    data: convertData(data.sort(function (a, b) {
		                        return b.value - a.value;
		                    }).slice(0, 6)),
		                    symbolSize: function (val) {
		                        return val[1] /2;
		                    },
		                    showEffectOn: 'render',
		                    rippleEffect: {
		                        brushType: 'stroke'
		                    },
		                    hoverAnimation: true,
		                    label: {
		                        normal: {
		                            formatter: '{b}',
		                            position: 'right',
		                            show: true
		                        }
		                    },
		                    itemStyle: {
		                        normal: {
		                            color: '#f4e925',
		                            shadowBlur: 10,
		                            shadowColor: '#333'
		                        }
		                    },
		                    zlevel: 1
		                }
		            ]
		        };
		        echartslayer = new ol3Echarts(option, {
		            stopEvent: false,
		            hideOnMoving: false,
		            hideOnZooming: false,
		            forcedPrecomposeRerender: true,
		        });
		        echartslayer.appendTo(map)
		var btn5=document.getElementById("btn5")
		btn5.onclick=function(){
			echartslayer.remove()
		}
			}
})
