$(function () {
   
    ceshis1();
    ceshis2();
   

    function ceshis1() { 
       
		
		axios({
			method:"GET",
			url:'http://localhost:3000/api/sysuser/GDP'
		// 	params: {
		//   keyword: this.keyword,
		//   pageIndex: this.pageIndex,
		//   pageSize: this.pageSize
		// }
		}).then(response => {
			console.log(response);
			if (response.status == 200) {
				var myChart = echarts.init(document.getElementById('chart'));
				var areaList=[
					"上海2010年", "上海2020年","云南2010年", "云南2020年","内蒙古2010年", "内蒙古2020年","北京2010年", "北京2020年",
					"吉林2010年", "吉林2020年","四川2010年", "四川2020年", "天津2010年", "天津2020年","宁夏2010年", "宁夏2020年", 
					"安徽2010年", "安徽2020年","山东2010年", "山东2020年","山西2010年", "山西2020年","广东2010年", "广东2020年",
					"广西2010年", "广西2020年","新疆2010年", "新疆2020年","江苏2010年","江苏2020年", "江西2010年", "江西2020年",
					"河北2010年","河北2020年","河南2010年","河南2020年","浙江2010年", "浙江2020年","海南2010年", "海南2020年",
					"湖北2010年", "湖北2020年", "湖南2010年", "湖南2020年", "甘肃2010年", "甘肃2020年", "福建2010年", "福建2020年",
					"西藏2010年", "西藏2020年","贵州2010年", "贵州2020年","辽宁2010年", "辽宁2020年","重庆2010年", "重庆2020年",
					"陕西2010年", "陕西2020年", "青海2010年", "青海2020年","黑龙江2010年", "黑龙江2020年"
				]
				var arr=[];
			    let datas = response.data
				var data=datas.data
				for(var i=0;i<31;i++){
					var d=data[i];
					var Arr=[]
					var a=d["2010年人均GDP"]
					var b=d["2020年人均GDP"]
					Arr=[a,b];
					arr.push.apply(arr,Arr)
				}
				console.log(arr)
				var displayNum = screen.width > 1500 ? 4 : 2;//每次显示数据条数
				var groupNum = Math.ceil(arr.length / displayNum);
				var arrGroup = [];
				for (var i = 0; i <arr.length; i += displayNum) {//数据按个数分组存储
				    arrGroup.push(arr.slice(i, i + displayNum));
				}
				var areaGroup = [];
				for (var i = 0; i < areaList.length; i += displayNum) {//区域名称按个数分组存储
				    areaGroup.push(areaList.slice(i, i + displayNum));
				}
				var groupOption = []; 
				for (var i = 0; i < groupNum; i++) {
				    var temp = {
				        xAxis: [
				            {
				                type: 'category',
				                data: areaGroup[i],
				                axisLabel: {
				                    show: true,
				                    textStyle: {color: 'rgba(255,255,255,0.6)',fontSize: 14},
				                    interval: 0,
				                },
				                splitLine: { show: false },
				            }
				        ],
				        series: [
				            { data: arrGroup[i] },
				        ]
				    };
				    groupOption.push(temp);
				}
				option = {
				    baseOption: {
				        color: ['#2ea5c3', '#e08f68'],
				        timeline: {
				             // y: 0,
				             axisType: 'category',
				             show:false,
				             //realtime: false,
				             //loop: false,
				             autoPlay: true,
				             //currentIndex: 2,
				             playInterval: 3000,
				             controlStyle: {show:false},
				             data: areaGroup,
				             label: {formatter : function(s) {return '';}
				        }
				    },
				    tooltip: {},
				    calculable : true,
				    grid: {
				        x:60,
				        top: 30,
				        bottom: 30,
				    },
				    xAxis: [
				        {
				            type:'category',
				            axisLabel:{interval: 0},
				            data:areaList,
				            splitLine: {show: false},
				            axisTick: {             
				                show: false//刻度线不显示
				            },
				            axisLine: {
				                lineStyle: {color: '#0C1A5B'}
				            },
				        }
				    ],
				    yAxis: [
				        {
				            minInterval: 100,//设置最小刻度值
				            type: 'value',
				            max: 180000,
				            splitLine: {
				                show: true,
				                lineStyle: {
				                    color: 'rgba(255,255,255,0.1)'
				                }
				            },
				            axisTick: {show: false},
				            axisLine: {
				                lineStyle: {color: '#0C1A5B'}
				            },
				            axisLabel: {
				                textStyle: {
				                    fontSize: 14
				                },
				                formatter: function(value) {
				                    return value
				                },
				                color: 'rgba(255,255,255,0.6)'
				            }
				        }
				    ],
				    series: [
				                {
				                    name: '人均GDP',
				                    type: 'bar',
				                    barWidth: 30, label: {
				                        normal: {
				                            show: true,
				                            position: 'top',
				                            color: '#D0D8E2',
				                        }
				                    },
				                data: arr,
				                itemStyle: {
				                    normal: {
				                        color: new echarts.graphic.LinearGradient(0, 0, 0, 1,
				                             [{offset: 0,color: '#75c0ea'},
				                             {
				                                offset: 1,
				                                color: '#3b85dc'
				                            }
				                        ]
				                        )
				                    }
				                }
				            }
				        ]
				    },
				    options: groupOption
				};
				myChart.setOption(option);
				window.addEventListener("resize",function(){
				    myChart.resize();
				})
		
			}
			 else{
				 console.log(response)
			    }
			})
			.catch(error => {
			    console.log(error)
			})   
		}




    function ceshis2() {
        var myChart = echarts.init(document.getElementById('line'));
        var option = {
            tooltip: {
                trigger: 'axis',
                formatter: '{b0}:<br>{c0}万人'
            },
            legend: {
                icon: 'rect',
                itemWidth: 14,itemHeight: 5,itemGap:10,
                data: ['贫困人口'],
                right: '10px',
                top: '0px',
                textStyle: {
                    fontSize: 12,
                    color: '#fff'
                }
            },
            grid: {
                x:60,
                top: 30,
                bottom: 30,
            },
            xAxis: [{
                type: 'category',
                boundaryGap: false,
                axisLine: {
                    lineStyle: {color: '#57617B'}
                },
                axisLabel: {
                    textStyle: {color:'#fff'}
                },
                axisTick: {
                    show: false
                },
                data:[
                    "2012年","2013年","2014年","2015年","2016年","2017年","2018年","2019年","2020年"
                ]
            }],
            yAxis: [{
                type: 'value',
                minInterval: 100,
                min:0,
                max:9900,
                splitLine: {
                    show: true,
                    lineStyle: {
                        color: 'rgba(255,255,255,0.1)'
                    }
                },
                axisTick: {show: false},
                axisLine: {
                    lineStyle: {color: '#0C1A5B'}
                },
                axisLabel: {
                    textStyle: {
                        fontSize: 14
                    },
                    formatter: function(value) {return value},
                    color: 'rgba(255,255,255,0.6)'
                    }
            }],
            series: [{
                name: '贫困人口',
                type: 'line',
                smooth: true,
                lineStyle:
                 {
                     normal: {width: 2}
                    },
                yAxisIndex:0,
                itemStyle: {normal: { color: '#B996F8'}},
                data: [
                    "9899","8249","7017","5575","4335","3046","1660","511","0"
                ],
            }]
        }
        myChart.setOption(option);
        window.addEventListener("resize",function(){
            myChart.resize();
        })
    }
});