$(function () {

    var btn2 = document.getElementById("btn2")
    var echartsLayer;
    btn2.onclick = function () {
        echartsLayer = new EChartsLayer(getOption());
        function getOption() {
            geoCoordMap = {
                '北京': [116.4551, 40.2539],
                '广东': [113.5107, 23.2196],
                '广西': [108.479, 23.1152],
                '西藏': [91.1172, 29.6469],
                '深圳': [114.0661, 22.5485],
                '江西': [116.0046, 28.6633],
                '黑龙江': [127.9688, 45.368],
                '新疆': [87.9236, 43.5883],
                '青海': [101.4038, 36.8207],
                '内蒙古': [110.3467, 41.4899],
                '辽宁': [123.1238, 42.1216],
                '河南': [113.4668, 34.6234],
                '湖北': [114.3896, 30.6628],
                '河北': [114.4995, 38.1006],
                '海南': [110.3893, 19.8516],
                '陕西': [109.1162, 34.2004],
                '贵州': [106.6992, 26.7682],
                '江苏': [118.8062, 31.9208],
                '天津': [117.4219, 39.4189],
                '重庆': [108.384366, 30.439702],
                '甘肃': [103.5901, 36.3043],
                '浙江': [119.5313, 29.8773],
                '四川': [103.9526, 30.7617],
                '吉林': [125.8154, 44.2584],
                '上海': [121.4648, 31.2891],
                '云南': [102.9199, 25.4663],
                '宁夏': [106.3586, 38.1775],
                '福建': [119.4543, 25.9222],
            };
            var BJData = [
                [{
                    name: '北京'
                }, {
                    name: '北京',
                    value: 160
                }],
                [{
                    name: '北京'
                }, {
                    name: '内蒙古',
                    value: 60
                }],
                [{
                    name: '北京'
                }, {
                    name: '新疆',
                    value: 60
                }],
                [{
                    name: '北京'
                }, {
                    name: '西藏',
                    value: 60
                }],
                [{
                    name: '北京'
                }, {
                    name: '青海',
                    value: 60
                }],
                [{
                    name: '北京'
                }, {
                    name: '辽宁',
                    value: 60
                }],
                [{
                    name: '北京'
                }, {
                    name: '河南',
                    value: 60
                }],
                [{
                    name: '北京'
                }, {
                    name: '湖北',
                    value: 60
                }],
                [{
                    name: '北京'
                }, {
                    name: '河北',
                    value: 60
                }]
            ];
            var JSData = [
                [{
                    name: '江苏'
                }, {
                    name: '江苏',
                    value: 160
                }],
                [{
                    name: '江苏'
                }, {
                    name: '陕西',
                    value: 60
                }],
                [{
                    name: '江苏'
                }, {
                    name: '青海',
                    value: 60
                }],
                [{
                    name: '江苏'
                }, {
                    name: '海南',
                    value: 60
                }],
                [{
                    name: '江苏'
                }, {
                    name: '贵州',
                    value: 60
                }],
                [{
                    name: '江苏'
                }, {
                    name: '西藏',
                    value: 60
                }],
                [{
                    name: '江苏'
                }, {
                    name: '新疆',
                    value: 60
                }]
            ];
            var TJData = [
                [{
                    name: '天津'
                }, {
                    name: '天津',
                    value: 160
                }],
                [{
                    name: '天津'
                }, {
                    name: '甘肃',
                    value: 60
                }],
                [{
                    name: '天津'
                }, {
                    name: '河北',
                    value: 60
                }],
                [{
                    name: '天津'
                }, {
                    name: '青海',
                    value: 60
                }],
                [{
                    name: '天津'
                }, {
                    name: '西藏',
                    value: 60
                }],
                [{
                    name: '天津'
                }, {
                    name: '新疆',
                    value: 60
                }],
                [{
                    name: '天津'
                }, {
                    name: '重庆',
                    value: 60
                }],
                [{
                    name: '天津'
                }, {
                    name: '陕西',
                    value: 60
                }],
            ];
            var ZJData = [
                [{
                    name: '浙江'
                }, {
                    name: '浙江',
                    value: 160
                }],
                [{
                    name: '浙江'
                }, {
                    name: '贵州',
                    value: 60
                }],
                [{
                    name: '浙江'
                }, {
                    name: '吉林',
                    value: 60
                }],
                [{
                    name: '浙江'
                }, {
                    name: '湖北',
                    value: 60
                }],
                [{
                    name: '浙江'
                }, {
                    name: '四川',
                    value: 60
                }]
            ];
            var SHData = [
                [{
                    name: '上海'
                }, {
                    name: '上海',
                    value: 160
                }],
                [{
                    name: '上海'
                }, {
                    name: '云南',
                    value: 60
                }],
                [{
                    name: '上海'
                }, {
                    name: '新疆',
                    value: 60
                }],
                [{
                    name: '上海'
                }, {
                    name: '西藏',
                    value: 60
                }],
                [{
                    name: '上海'
                }, {
                    name: '青海',
                    value: 60
                }],
                [{
                    name: '上海'
                }, {
                    name: '贵州',
                    value: 60
                }],
                [{
                    name: '上海'
                }, {
                    name: '湖北',
                    value: 60
                }],
                [{
                    name: '上海'
                }, {
                    name: '重庆',
                    value: 60
                }],
            ];
            var FJData = [
                [{
                    name: '福建'
                }, {
                    name: '福建',
                    value: 160
                }],
                [{
                    name: '福建'
                }, {
                    name: '宁夏',
                    value: 60
                }],
            ]
            var SZData = [
                [{
                    name: '深圳'
                }, {
                    name: '深圳',
                    value: 160
                }],
                [{
                    name: '深圳'
                }, {
                    name: '广东',
                    value: 60
                }],
                [{
                    name: '深圳'
                }, {
                    name: '广西',
                    value: 60
                }],
                [{
                    name: '深圳'
                }, {
                    name: '西藏',
                    value: 60
                }],
                [{
                    name: '深圳'
                }, {
                    name: '江西',
                    value: 60
                }],
                [{
                    name: '深圳'
                }, {
                    name: '黑龙江',
                    value: 60
                }],
                [{
                    name: '深圳'
                }, {
                    name: '新疆',
                    value: 60
                }]
            ];
            var planePath = 'path://M.6,1318.313v-89.254l-319.9-221.799l0.073-208.063c0.521-84.662-26.629-121.796-63.961-121.491c-37.332-0.305-64.482,36.829-63.961,121.491l0.073,208.063l-319.9,221.799v89.254l330.343-157.288l12.238,241.308l-134.449,92.931l0.531,42.034l175.125-42.917l175.125,42.917l0.531-42.034l-134.449-92.931l12.238-241.308L1705';
            // push进去线路开始-结束地点-经纬度
            var convertData = function (data) {
                var res = [];
                for (var i = 0; i < data.length; i++) {
                    var dataItem = data[i];
                    var fromCoord = geoCoordMap[dataItem[0].name];
                    var toCoord = geoCoordMap[dataItem[1].name];
                    if (fromCoord && toCoord) {
                        res.push({
                            fromName: dataItem[0].name,
                            toName: dataItem[1].name,
                            coords: [fromCoord, toCoord]
                        });
                    }
                }
                return res;
            };
            var color = ['#a6c84c', '#ffa022', '#46bee9', '#BA55D3', '#FFB6C1', '#20B2AA', '#EE82EE']; //颜色
            var series = [];
            [
                ['北京', BJData],
                ['江苏', JSData],
                ['天津', TJData],
                ['浙江', ZJData],
                ['上海', SHData],
                ['福建', FJData],
                ['深圳', SZData],
            ].forEach(function (item, i) {
                console.log(convertData(item[1])) // 打印线路
                series.push({
                        name: item[0], // 系列名称，用于tooltip的显示
                        type: 'lines',
                        zlevel: 1, //用于 Canvas 分层，不同zlevel值的图形会放置在不同的 Canvas 中
                        //出发到目的地的白色尾巴线条，线特效的配置
                        effect: {
                            show: true,
                            period: 6, // 特效动画的时间，单位为 s             
                            trailLength: 0.7, // 特效尾迹的长度。取从 0 到 1 的值，数值越大尾迹越长                     
                            color: '#fff', // 特效标记的颜色                   
                            symbolSize: 3 // 特效标记的大小
                        },
                        //出发到目的地的线条颜色
                        lineStyle: {
                            normal: {
                                color: color[i],
                                width: 0,
                                curveness: 0.2
                            }
                        },
                        data: convertData(item[1]) //开始到结束数据
                    },
                    //出发地信息
                    {
                        name: item[0],
                        type: 'lines',
                        coordinateSystem: 'geo',
                        zlevel: 2,
                        rippleEffect: {
                            brushType: 'stroke'
                        },
                        label: {
                            normal: {
                                show: false,
                                position: 'right',
                                formatter: '{a}'
                            }
                        },
                        effect: {
                            show: true,
                            period: 6,
                            trailLength: 0,
                            symbol: planePath,
                            symbolSize: 15
                        },
                        lineStyle: {
                            normal: {
                                color: color[i],
                                width: 1,
                                opacity: 0.4,
                                curveness: 0.2
                            }
                        },
                        data: convertData(item[1])
                    },
                    // 目的地信息
                    {
                        name: item[0] + '帮扶地区',
                        type: 'effectScatter',
                        coordinateSystem: 'geo',
                        zlevel: 2,
                        rippleEffect: {
                            brushType: 'stroke'
                        },
                        label: {
                            normal: {
                                show: true,
                                position: 'right',
                                formatter: '{b}'
                            }
                        },
                        symbolSize: function (val) {
                            return val[2] / 8;
                        },
                        itemStyle: {
                            normal: {
                                color: color[i]
                            }
                        },
                        data: item[1].map(function (dataItem) {
                            return {
                                name: dataItem[1].name,
                                value: geoCoordMap[dataItem[1].name].concat([dataItem[1].value])
                            };
                        }),
                        
                    });
            });
            return{
                tooltip:{
                    trigger: 'item'
                },
                series: series,
                geo: {
                    type: 'map',
                    map: 'map',
                    label: { //鼠标移入是否显示省份
                        emphasis: {
                            show: true
                        }
                    },
                    roam: true,
                    itemStyle: {
                        normal: {
                            borderColor: 'rgba(147, 235, 248, 1)',
                            borderWidth: 1,
                            areaColor: {
                                type: 'radial',
                                x: 0.5,
                                y: 0.5,
                                r: 0.8,
                                colorStops: [{
                                    offset: 0,
                                    color: 'rgba(147, 235, 248, 0)'
                                }, {
                                    offset: 1,
                                    color: 'rgba(147, 235, 248, .2)'
                                }],
                                globalCoord: false
                            },
                            shadowColor: 'rgba(128, 217, 248, 1)',
                            shadowOffsetX: -2,
                            shadowOffsetY: 2,
                            shadowBlur: 10
                        },
                        emphasis: {
                            areaColor: "#389BB7",
                            borderWidth: 0
                        },
                    }
                },
            }
           
        }
        echartsLayer.appendTo(map)
		var btn5=document.getElementById("btn5")
		btn5.onclick=function(){
			echartsLayer.remove()
		}
		
    }
	
    
})