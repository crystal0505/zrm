$(function () {
  
    var btn4 = document.getElementById("btn4");
      btn4.onclick = function () {
       

        var container = document.getElementById("popup");
		container.style.display="";
        var content = document.getElementById("popup-content");
        var closer = document.getElementById("popup-closer");
        var wmssource = new ol.source.Vector({
          projection: "EPSG:4326",
          url: "http://localhost/map/poor.geojson", 
          format: new ol.format.GeoJSON(),
          crossOrigin: "*",
        });

        var poorlayer = new ol.layer.Vector({
          source: wmssource,
        });
        map.addLayer(poorlayer);

        var overlay = new ol.Overlay({
          element: container,
          autoPanAnimation: {
            duration: 250,
          },
        });
        map.addOverlay(overlay);

        closer.onclick = function () {
          overlay.setPosition(undefined);
          return false;
        };
        map.on("click", function (e) {
          var pixel = map.getEventPixel(e.originalEvent);

          //forEachFeatureAtPixel的原理，是遍历操作
          var feature = map.forEachFeatureAtPixel(pixel, function (feature) {
            return feature;
          });
          console.log(feature);
          var coordinate = e.coordinate;
          content.innerHTML =
            "<p>信息：</p><code>" +
            "贫困县名称：" +
            feature.values_.县 +
            "<br>县类型："+
            feature.values_.县类型+
            "<br>县代码："+
            feature.values_.县代码+
            // "<br>脱贫时间："+
            // feature.values_.脱贫时间+
            "<br>所属省份:" +
            feature.values_.省 +
            "<br>省代码：" +
            feature.values_.省代码 +
            "<br>所属市区:" +
            feature.values_.市+
            "<br>市代码："+
            feature.values_.市代码+
            "<br>扶贫产业："+
            feature.values_.扶贫产业+
			"<br>脱贫时间："+
			feature.values_.脱贫时间
            "</code>";
          overlay.setPosition(coordinate);
        });
		var btn5=document.getElementById("btn5")
		btn5.onclick=function(){
			map.removeLayer(overlay)
			map.removeLayer(poorlayer)
			
		};
      };
    
})